import os
import re
import sys
import smcap
from pprint import pprint, pformat
import subprocess
import logging
import urllib


sys.path.append("/usr/share/smithproxy/infra")
from flask import Flask, render_template, request, send_file
import pylibconfig2 as cfg
import daemon


SMITHWEB_PIDFILE="/var/run/smithproxy_capweb.pid"
SMITHWEB_LOGFILE="/var/log/smithproxy_capweb.log"
SMITHWEB_VERSION = "0.1.0"
g_capture_path = None
g_ca_cert = None
g_ca_key = None


def handle_error(request):
    logging.error("default handler: request error:" + request.url)
    return "<h3>Oops..!</h3><p> Something went wrong while requesting resource:</p>" + request.url

def make_tree(path):
    #print "Current path = " + path
    tree = dict(name=os.path.basename(path), path=path,children=[],di=True)
    try: lst = os.listdir(path)
    except OSError,e:
        logging.error("OSerror (" + path +"): " + str(e))
    else:
        for name in reversed(sorted(lst)):
            fn = os.path.join(path, name)
            safe_path = urllib.quote(fn)
            
            logging.debug("make_tree: " + name + " -> " + fn)
            
            if os.path.isdir(fn):
                #tree['children'].append(make_tree(fn))
                # tree['children'].append(dict(name=name,path=path+"/"+name,di=True,mitm_info=None,size=0))
                tree['children'].append(dict(name=name,path=fn,di=True,mitm_info=None,size=0,safe_path=safe_path))
            else:
                
                s = str(os.path.getsize(fn))
                logging.debug(">%s<" % (name,))
                
                if name.endswith('smcap'):
                    mitm_info = {}
                    #m = re.search("(?P<time>\d+-\d+-\d+)_([a-z\+]+)_(?P<src>\d+\.\d+\.\d+\.\d+):(?P<sport>\d+)-([a-z\+]+)_(?P<dst>\d+\.\d+\.\d+\.\d+):(?P<dport>\d+)",name)
                    m = re.search("(?P<time>\d+-\d+-\d+)_([a-z\+]+)_(?P<src>\d+\.\d+\.\d+\.\d+):(?P<sport>\d+)(?P<user>_u_[a-z_]+){0,1}-([a-z\+]+)_(?P<dst>\d+\.\d+\.\d+\.\d+):(?P<dport>\d+)",name)
                    if m:
                        logging.debug("make_tree: file info:")
                        logging.debug(m.group('time'))
                        logging.debug(m.group('src'))
                        logging.debug(m.group('sport'))
                        if('user' in m.groups()):
                            logging.debug(m.group('user'))
                        
                        logging.debug(m.group('dst'))
                        logging.debug(m.group('dport'))
                        
                        r = m.groupdict()
                        r['time'] = re.sub('-',':',r['time'])
                        
                        tree['children'].append(dict(name=name,path=path,di=False,mitm_info=r,size=s,safe_path=safe_path))
                    else:
                        tree['children'].append(dict(name=name,path=path,di=False,mitm_info=None,size=s,safe_path=safe_path))
                else:
                    tree['children'].append(dict(name=name,path=path,di=False,mitm_info=None,size=s,safe_path=safe_path))
                
    #pprint(tree)
    
    #tree['children'] = sorted(tree['children'])
    return tree



app = Flask(__name__)

@app.route('/captures')
def dirtree():
    path = os.path.expanduser(u"captures")
    logging.debug("Epanding " + path)
    return render_template('captures.html', tree=make_tree(path),relpath=u"captures")

@app.route('/')
def index():
    
    sm_version = "== !! NOT INSTALLED !! =="
    try:
        sm_version=subprocess.check_output(["smithproxy","-v"]).strip()
    except OSError,e:
        pass
    
    return render_template("index.html", sm_version=sm_version,sw_version=SMITHWEB_VERSION)

#@app.route('/stylesheets/CollapsibleLists.js')
#def style():
    #return send_file('stylesheets/CollapsibleLists.js',mimetype='text/javascript')


@app.route('/what-is-ssl-mitm.html')
def wism():
    return render_template("what-is-ssl-mitm.html")

@app.route("/pythonize")
def pythonize():
    fnm = urllib.unquote(request.values['a'])
    logging.info("/pythonize: " + fnm)
    
    exp_file = '/tmp/smithproxy_capweb/pythonize_output'
    
    try:
        smcap.export(fnm,exp_file)
        
        
        #html_out = re.sub('\n','</br>',txt)
        #return render_template("pythonize.html",txt=html_out, fnm=fnm)

        return send_file(exp_file,mimetype='text/x-script.python',as_attachment=True,attachment_filename="%s__pythonized.py" % (os.path.basename(fnm),))
    except IOError, e:
        logging.error("pythonize: exception caught: " + str(e))
    
    return handle_error(request)

@app.route('/smcap2pcap')
def smcap2pcap():
    fnm = urllib.unquote(request.values['a'])
    logging.info("/smcap2pcap: " + fnm)
    
    fnm = '/'.join(fnm.split('/')[1:])
    logging.info("/smcap2pcap file: " + fnm)
    
    fnm = '/'.join([g_capture_path,fnm])
    logging.info("/smcap2pcap full file path: " + fnm)
    
    o = None
    try:
        o = subprocess.check_output(["smcap2pcap",fnm]).strip()
    except OSError, e:
        logging.error("smcap2pcap exec problem: %s" % (str(e),))
        return handle_error(request)
    
    logging.info("/smcap2pcap: result: " + o)
    if o:
        return send_file(o,mimetype='application/vnd.tcpdump.pcap',as_attachment=True,attachment_filename="%s__replayed.pcap" % (os.path.basename(o),))
    
    return handle_error(request)

@app.errorhandler(404)
def not_found(error):
    logging.info(pformat(request.url))
    logging.info(pformat(request.base_url))
    logging.info(pformat(request.path))
    
    npath = os.path.normpath(request.path)
    logging.info(pformat(npath))
    
    if npath.startswith('/captures'):
        file_subpath = '/'.join(npath[1:].split('/')[1:])
        if os.path.isfile(npath[1:]):
            return send_file(os.path.join(g_capture_path, file_subpath),mimetype="text/plain")
        
        elif os.path.isdir(npath[1:]):
            r =  render_template('captures.html', tree=make_tree(npath[1:]))
            #print str(r)
            return r
    
    if npath == '/images':
        return send_file('static/'+request.values['a'],mimetype='image/png')

    if npath == "/cacert":
        return send_file(g_ca_cert,mimetype='application/x-x509-ca-cert',as_attachment=True,attachment_filename="smithproxy_ca_cert.pem")
    
    if npath == "/srvkey":
        return send_file(g_srv_key,mimetype='application/x-pem-file',as_attachment=True,attachment_filename="smithproxy_decrypt_key.pem")
    
    if npath == '/certs':
        return send_file('static/'+request.values['a'],mimetype='application/x-x509-ca-cert',as_attachment=True,attachment_filename=request.values['a'])

    if npath == '/keys':
        return send_file('static/'+request.values['a'],mimetype='application/x-pem-file',as_attachment=True,attachment_filename=request.values['a'])


    if npath == '/styles':
        return send_file('stylesheets/'+request.values['a'],mimetype='text/css')

    
    return handle_error(request)


class SmithWeb(daemon.Daemon):
    def start(self):
        logging.info("starting")
        daemon.Daemon.start(self)

    def stop(self):
        logging.info("terminating")
        daemon.Daemon.stop(self)
        
        
    def run(self):
        app.run(host='0.0.0.0', port=80, debug=False)

if __name__=="__main__":
    logging.basicConfig(filename=SMITHWEB_LOGFILE, level=logging.INFO, format='%(asctime)s [%(process)d] [%(levelname)s] %(message)s')
    
    c = cfg.Config()
    c.read_file("/etc/smithproxy/smithproxy.cfg")
    g_capture_path = c.settings.write_payload_dir
    g_ca_cert = c.settings.certs_path + "ca-cert.pem"
    g_srv_key = c.settings.certs_path + "srv-key.pem"
    
    try:
        os.mkdir("/tmp/smithproxy_capweb")
        logging.info("tmpdir created")
    except OSError,e:
        logging.error("main: exception caught: " + str(e))

    try:
        os.chdir("/tmp/smithproxy_capweb")
        logging.info("chdir to tmpdir ok")
    except OSError,e:
        logging.error("main: exception caught: " + str(e))
        
    try:
        os.unlink(u"captures")
        logging.info("captures unlinked")
    except OSError,e:
        logging.error("main: exception caught: " + str(e))

    try:
        os.symlink(g_capture_path,"captures")
        logging.info("captures symlinked")
    except OSError,e:
        logging.error("main: exception caught: " + str(e))
    
    
    #app.static_dir.append("stylesheets")
    logging.info("Smithweb %s - capture web starting..." % (SMITHWEB_VERSION,))
    logging.debug("Current dir: " + os.getcwd())
    logging.debug("CA cert: " + g_ca_cert)
    logging.debug("Srv key: " + g_srv_key)

    if len(sys.argv) > 1:
        if "start" == sys.argv[1]:
            logging.info("Daemonizing...")
            s = SmithWeb("smithproxy_capweb",SMITHWEB_PIDFILE)
            s.pwd = "/tmp/smithproxy_capweb"
            s.start()
            logging.info("Done")
        elif "run" == sys.argv[1]:
            logging.info("Running in foreground ...")
            s = SmithWeb("smithproxy_capweb",SMITHWEB_PIDFILE)
            s.run()
        
        elif "stop" == sys.argv[1]:
            logging.info("Stopping daemon...")
            s = SmithWeb("smithproxy_capweb",SMITHWEB_PIDFILE)
            s.stop()
            
        
    else:
        msg = "Usage: " + sys.argv[0] + " [start|run|stop]"
        msg2 = "start: daemonize\nrun: run in the foreground\nstop: stop daemon"
        
        logging.error(msg)
        print msg
        print msg2
        
    
    
    